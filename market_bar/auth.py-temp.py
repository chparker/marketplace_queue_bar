import base64

# ecosystem.atlassian.net credentials
eco_username = "USERNAME"
eco_password = "PASSWORD"

# marketplace.atlassian.net credentials
mpac_username = "USERNAME"
mpac_password = "PASSWORD"

def combine(username, password):
    combi = username+":"+password
    user_auth = base64.b64encode(combi.encode('utf-8'))
    final_user_auth = str(user_auth)
    final_user_auth = final_user_auth[2:-1]
    return(final_user_auth)

#combine(mpac_username, mpac_password)
