#!/usr/local/bin/python3
# <bitbar.title>Marketplace Support Quick Glance</bitbar.title>
# <bitbar.version>v0.1</bitbar.version>
# <bitbar.author>Chris Parkere</bitbar.author>
# <bitbar.desc>Quick view of Support and Approval Queue</bitbar.desc>
# <bitbar.dependencies>python</bitbar.dependencies>
#
# by C.J. Parker

import requests
from lxml import html
import json
from bs4 import BeautifulSoup
import market_bar.auth as auth

# Global Variables
ecosystem_url = "https://ecosystem.atlassian.net"
sp_queue = "https://ecosystem.atlassian.net/issues/?jql=project%20in%20(%22Atlassian%20Marketplace%22%2C%20%22Marketplace%20Vendor%20Support%22)%20AND%20type%20in%20(%22Support%20Request%22%2C%20%22Cloud%20Security%20Approval%22)%20AND%20status%20in%20(Open%2C%20Triage%2C%20%22Awaiting%20approval%22)%20AND%20assignee%20is%20EMPTY"
ap_queue = "https://marketplace.atlassian.com/admin/review/apps"

# APPROVAL QUEUE
#Y2hwYXJrZXJAYXRsYXNzaWFuLmNvbTpZc25wMTcyOQ==
def approval_queue_url():
    global site
    url = "https://marketplace.atlassian.com/admin/review/apps"
    headers = {
        'authorization': "Basic {0}".format(auth.combine(auth.mpac_username, auth.mpac_password)),
        'cache-control': "no-cache",
        }
    response = requests.request("GET", url, headers=headers)
    site = response.content
    return approval_queue(site)


def approval_queue(site):
    global approval_count
    apps = []
    soup = BeautifulSoup(site, "lxml")
    rows = soup.table.tbody
    for row in rows:
        apps.append(row)
    approval_count = len(apps)
    return approval_count


# SUPPORT QUEUE

def support_queue():
    global issue_keys
    searchUrl = "https://ecosystem.atlassian.net/rest/api/2/search?jql=project+in+(%22Atlassian+Marketplace%22,+%22Marketplace+Vendor+Support%22)+AND+type+in+(%22Support+Request%22,+%22Cloud+Security+Approval%22)+AND+status+in+(Open,+Triage,+%22Awaiting+approval%22)+AND+assignee+is+EMPTY"
    headers = {
    'authorization': "Basic {0}".format(auth.combine(auth.eco_username, auth.eco_password)),
    'Content-Type': 'application/json',
    }
    response = requests.get(searchUrl, headers=headers)
    placeholder = response.content
    jsonResults = json.loads(placeholder)
    issue_keys = []
    for issuekey in jsonResults['issues']:
        issue_keys.append((issuekey['key']))
    issue_keys = len(issue_keys)
    return issue_keys
    # extra code that creates a drop down list with clickable links to all the support tickets
    """print("---")
    if len(issue_keys) < 20:
        for i in issue_keys:
            #clickable = ecosystem_url + "/browse/" + i
            print('{} | href=https://ecosystem.atlassian.net/browse/{}'.format(i,i))
    else:
        print('All Issues| href=https://ecosystem.atlassian.net/issues/?filter=45702')"""


# Funcito  to print for bit bar

def printer(issue_keys, approval_count):
    print("Support Queue:", issue_keys,"...","Approval Queue:", approval_count)
    print("---")
    print("Support Queue | href={}".format(sp_queue))
    print("Approval Queue | href={}".format(ap_queue))


#get_search_url()
support_queue()
approval_queue_url()
printer(issue_keys, approval_count)
