# README #

Let's get this set up.

![Example](https://preview.ibb.co/gaUx46/Screen_Shot_2018_01_18_at_15_32_00.png)

### What is this repository for? ###

This repo uses [BitBar](https://github.com/matryer/bitbar) to display the queue status for the Marketplace Support Team.

### How do I get set up? ###

Before setting this up the following dependencies are required:



* Python3
* BitBar
	* Download instructions [here](https://getbitbar.com/)
* Python modules - run 'pip3 install <module_name>'
	* requests
	* lxml
	* json
	* [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
		* 	e.g. pip3 install bs4	

**BitBar Specifics**

- Installer: https://github.com/matryer/bitbar/releases/tag/v1.9.2
- Just unzip and drop it into the Application folder
- Readme: https://github.com/matryer/bitbar
- It's important to make a note of the 'plugin folder' which you select during set up, this is where the script will need to be placed.


### Deployment instructions ###

Within the sub directory __market_bar/__ there is a file name **auth.py-temp**, you'll need to make a copy of this and remove the "-temp" portion, then edit the file so that your username and password is added for both ecosystem.atlassian.net and marketplace.atlassian.net. 

Once the **auth.py** is set up you need to copy both the market_bar.5m.py file and the market_bar directory (and its children) to the plugin folder you selected for BitBar.

_cp -R market_bar market_bar.5m.py {location\_of\_plugin\_folder}_

The last thing that needs to be done is to make all the files executable, we can do this with the following command while in the root of the plugin folder directory:

_chmod 755 market\_bar/* market\_bar.5m.py_

### More Deets ###

The *5m* part in the file name determines when the refresh rate of the script. See the [BitBar Readme](https://github.com/matryer/bitbar) for more details.

### Who do I talk to? ###

* chparker@atlassian.com